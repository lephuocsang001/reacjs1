import React, { Component } from "react";

export default class BodyComponent extends Component {
  render() {
    return (
      <div>
        <div
          className="container w-75 mt-3 p-5"
          style={{ backgroundColor: "#E8ECEF" }}
        >
          <h1 className="text-left">A Warm Welcome!</h1>
          <p className="text-left">
            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Id
            similique cupiditate vero nisi, ipsum accusantium culpa asperiores
            et repellat. Necessitatibus dolorum voluptate delectus debitis
            expedita veritatis. Eius nihil tempora eveniet.
          </p>
          <div className="text-left">
            <button className="btn btn-primary">Call to action!</button>
          </div>
        </div>
      </div>
    );
  }
}
