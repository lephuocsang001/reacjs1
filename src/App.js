import "./App.css";
import BodyComponent from "./BodyComponent";
import CardComponent from "./CardComponent";
import FooterComponent from "./FooterComponent";
import HeaderComponent from "./HeaderComponent";

function App() {
  return (
    <div className="App">
      <HeaderComponent />
      <BodyComponent />
      <CardComponent />
      <FooterComponent />
    </div>
  );
}

export default App;
