import React, { Component } from "react";

export default class FooterComponent extends Component {
  render() {
    return (
      <div className="bg-dark">
        <div
          className="container mt-3 bg-dark text-light"
          style={{ height: "100px", lineHeight: "100px" }}
        >
          Copyright &copy; Your Website 2019
        </div>
      </div>
    );
  }
}
